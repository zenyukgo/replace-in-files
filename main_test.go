package main

import (
	"testing"
)

func TestGetIgnoredWords(t *testing.T) {
	// arrange
	input := "ignored1\nignored2\nignored3"
	inBytes := []byte(input)

	// act
	output := getIgnoredWords(inBytes)

	// assert
	if len(output) != 3 {
		t.Error("wrong number elemented returned")
	}

	_, ok1 := output["ignored1"]
	_, ok3 := output["ignored3"]
	if !ok1 || !ok3 {
		t.Error("incorrect items returned")
	}

}

func TestGetWordTobeReplaced(t *testing.T) {
	// arrange
	input := "<myxmltag myxmlattribute=\"tobe_replaced\" anotherattribute=\"should_not_be_changed\" />\n<myxmltag myxmlattribute=\"should_not_be_changed2\" anotherattribute=\"should_not_be_changed3\" />"
	inRunes := []rune(input)
	ignoredWords := make(map[string]struct{})
	ignoredWords["should_not_be_changed"] = struct{}{}
	ignoredWords["should_not_be_changed2"] = struct{}{}
	ignoredWords["extra_one"] = struct{}{}

	// act
	output := getWordTobeReplaced(inRunes, ignoredWords, "myxmlattribute=\"")

	// assert
	_, ok := output["tobe_replaced"]
	if len(output) != 1 ||
		!ok {
		t.Error("getWordTobeReplaced failed")
	}
}
